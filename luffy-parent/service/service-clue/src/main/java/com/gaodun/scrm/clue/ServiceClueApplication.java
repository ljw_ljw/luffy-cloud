package com.gaodun.scrm.clue;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.gaodun.scrm"})
@EnableFeignClients(basePackages = {"com.gaodun.scrm"})
@EnableDiscoveryClient
public class ServiceClueApplication {

}
