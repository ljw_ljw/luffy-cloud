package com.gaodun.scrm.clue.redis;

import com.gaodun.scrm.clue.util.CacheHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class MessageReceive {

    public void receiveMessage(String message){

        System.out.println("----------收到消息了message：" + message);
        if(!StringUtils.isEmpty(message)){
            //在数据导入redis的时候，顺便推给所有集群服务同步本地缓存状态
            message = message.replaceAll("\"","");
            String[] split = StringUtils.split(message, ":");
            if(split == null || split.length == 2){
                CacheHelper.put(split[0], split[1]);
            }
        }
    }

}
