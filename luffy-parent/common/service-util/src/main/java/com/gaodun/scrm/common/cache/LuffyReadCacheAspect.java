package com.gaodun.scrm.common.cache;

import com.gaodun.scrm.common.constant.RedisConst;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
@Slf4j
public class LuffyReadCacheAspect {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private RedisTemplate redisTemplate;

    //环绕通知 实现类逻辑缓存
    //找注解 切入点表达式 而不是找类 找方法
    @Around(value = "@annotation(com.gaodun.scrm.common.cache.LuffyReadCache)")
    public Object cacheData(ProceedingJoinPoint pjp){

        MethodSignature signature = (MethodSignature) pjp.getSignature();
        LuffyReadCache luffyReadCacheCache = signature.getMethod().getAnnotation(LuffyReadCache.class);

        String cacheKey = luffyReadCacheCache.prefix() + Arrays.asList(pjp.getArgs())+ RedisConst.SKUCACHE_SUFFIX;
        String cacheLock = luffyReadCacheCache.prefix() + Arrays.asList(pjp.getArgs())+ RedisConst.SKULOCK_SUFFIX;

        Class returnType = signature.getReturnType();//signature 指的是方法加参数的组合签名
        Object o = redisTemplate.opsForValue().get(cacheKey);
        if(null != o){
            return o;
        }
        RLock lock = redissonClient.getLock(cacheLock);
        try{
            boolean res = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX1, RedisConst.SKULOCK_EXPIRE_PX2, TimeUnit.SECONDS);
            if(res){
                Object result = pjp.proceed(pjp.getArgs());
                if(null == result){
                    result = returnType.newInstance();
                    //防缓存穿透
                    redisTemplate.opsForValue().set(cacheKey,result,3,TimeUnit.MINUTES);
                }else {
                    redisTemplate.opsForValue().set(cacheKey, result, RedisConst.SKUKEY_TIMEOUT, TimeUnit.SECONDS);
                }
                return result;

            }else {
                Thread.sleep(1000);
                return redisTemplate.opsForValue().get(cacheKey);
            }
        }catch (Throwable e){
//            e.printStackTrace();
        }finally {
            lock.unlock();
        }

        return null;

        //布隆过滤器的add操作需要写在Canal监听器，或者单独用Bean注入一个BloomFilter时把数据库数据添加进布隆过滤器了
//        RBloomFilter<String> bloomfilter = this.redissonClient.getBloomFilter("bloomfilter");
//        bloomfilter.tryInit(50l, 0.03);

    }



}
