package com.gaodun.scrm.common.cache;

import java.lang.annotation.*;

//限定只注在方法上
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LuffyReadCache {

    /**
     * 缓存key的前缀
     * @return
     */
    String prefix() default "luffyReadCache";

    /**
     * 设置缓存的有效时间
     * 单位：分钟
     * @return
     */
    int timeout() default 5;
    /**
     * 防止雪崩设置的随机值范围
     * @return
     */
    int random() default 5;
    /**
     * 防止击穿，分布式锁的key
     * @return
     */
    String lock() default "lock";
}
