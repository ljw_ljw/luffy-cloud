package com.gaodun.scrm.common.rabbitmq.constant;

public class RabbitConst {

    public static final String MQ_KEY_PREFIX = "mq:list";
    public static final int RETRY_COUNT = 3;
    public static final int OBJECT_TIMEOUT = 10;

    /**
     * 定时任务
     */
    public static final String EXCHANGE_DIRECT_TASK = "exchange.direct.task";
    public static final String ROUTING_TASK_1 = "seckill.task.1";
    public static final String QUEUE_TASK_1  = "queue.task.1";

}
